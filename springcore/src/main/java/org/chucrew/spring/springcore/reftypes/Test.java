package org.chucrew.spring.springcore.reftypes;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext(
				"org/chucrew/spring/springcore/reftypes/config.xml");
		Student student = (Student) context.getBean("student");
		System.out.println(student);

		ShoppingCart shoppingCart = (ShoppingCart) context.getBean("shoppingcart");
		System.out.println(shoppingCart);
	}
}
