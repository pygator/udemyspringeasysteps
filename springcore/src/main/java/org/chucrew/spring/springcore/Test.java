package org.chucrew.spring.springcore;


import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {

    public static void main(String[] args) {
        ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("config-pschema.xml");
        Employee emp = (Employee) ctx.getBean("emp");
        System.out.println("Get the ID: " + emp.getId());
        System.out.println("Get the Name: " + emp.getName());
    }
}
